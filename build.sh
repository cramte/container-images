#!/bin/sh
set -eu
export images="kind-toolbox"

echo Logging in to the registry...
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

for image in $images; do
    cd $image
    export image_url=$CI_REGISTRY_IMAGE/$image
    docker build -t $image_url .
    docker push "$image_url"
    cd ..
done

