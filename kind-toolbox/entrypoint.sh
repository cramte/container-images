#!/bin/bash

function help() {
    echo "$1" is missing.
    echo 'Run me like this:'
    echo 'docker run --rm -ti -v $(pwd)/kube:/root/.kube -v /usr/bin/docker:/usr/bin/docker -v /var/run/docker.sock:/var/run/docker.sock --net=host jlukcodi/kind-toolbox'
    exit 1
}

function check() {
    if [ ! -e "$1" ]; then
        help "$1"
    fi
}

check /var/run/docker.sock
check /root/.kube
check /usr/bin/docker
echo kind create cluster
bash
